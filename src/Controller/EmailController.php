<?php

namespace App\Controller;

use App\Entity\Email;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * @Route("/email", name="email")
 */
class EmailController extends Controller
{

    /**
     * @Route("/create")
     */
    public function create(Request $requestUsers){
        $entityManager = $this->getDoctrine()->getManager();
        $users = new Email();
        $form = $this->createFormBuilder($users)
            ->add('sender',TextType::class)
            ->add('recipient',TextType::class)
            ->add('object',TextType::class)
            ->add('message', TextareaType::class)
            ->add('submit',SubmitType::class)
            ->getForm();
        $form->handleRequest($requestUsers);
        $task = $form->getData();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($task);
            $entityManager->flush();
            return $this->redirect('/email');
        }


        return $this->render('email/create.html.twig', [
            'form' => $form->createView()
        ]);

    }
    /**
     * @Route("/", name="email")
     */
    public function index()
    {
        $email = $this->getDoctrine()->getRepository(Email::class)->findAll();
        return $this->render('email/index.html.twig', [
            'Mails' => $email,
        ]);
    }

    /**
     * @Route("/edit/{id}")
     */
    public function update(Request $request, $id){
        $entityManager = $this->getDoctrine()->getManager();
        $users = $this->getDoctrine()->getRepository(Email::class)->find($id);
        $form = $this->createFormBuilder($users)
            ->add('sender', TextType::class)
            ->add('recipient', TextType::class)
            ->add('object', TextType::class)
            ->add('message', TextType::class)
            ->add('save', SubmitType::class)
            ->getForm();//Récupération du formulaire
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            return $this->redirect('/users');
        }
        return $this->render('email/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/delete/{id}")
     */
    public function delete($id){
        $entityManager = $this->getDoctrine()->getManager();
        $product = $entityManager->getRepository(Email::class)->find($id);
        $entityManager->remove($product);
        $entityManager->flush();
        return $this->redirect('/email');
    }
}
