<?php

namespace App\Controller;

use App\Entity\Produits;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * @Route("/produits")
 */
class ProduitsController extends Controller
{

    /**
     * @Route("/create")
     */
    public function create(Request $requestUsers){
        $entityManager = $this->getDoctrine()->getManager();
        $users = new Produits();
        $form = $this->createFormBuilder($users)
            ->add('ProductName',TextType::class)
            ->add('ProductPrice', IntegerType::class)
            ->add('save',SubmitType::class)
            ->getForm();
        $form->handleRequest($requestUsers);
        $task = $form->getData();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($task);
            $entityManager->flush();
            return $this->redirect('/produits');
        }


        return $this->render('produits/create.html.twig', [
            'form' => $form->createView()
        ]);

    }

    /**
     * @Route("/")
     */
    public function index()
    {
        $produits = $this->getDoctrine()->getRepository(Produits::class)->findAll();
        return $this->render('produits/index.html.twig', [
            'produits' => $produits,
        ]);
    }

    /**
     * @Route("/edit/{id}")
     */
    public function update(Request $request, $id){
        $entityManager = $this->getDoctrine()->getManager();
        $users = $this->getDoctrine()->getRepository(Produits::class)->find($id);
        $form = $this->createFormBuilder($users)
            ->add('ProductName', TextType::class)
            ->add('ProductPrice', IntegerType::class)
            ->add('save', SubmitType::class)
            ->getForm();//Récupération du formulaire
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            return $this->redirect('/produits');
        }
        return $this->render('produits/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/delete/{id}")
     */
    public function delete($id){
        $entityManager = $this->getDoctrine()->getManager();
        $product = $entityManager->getRepository(Produits::class)->find($id);
        $entityManager->remove($product);
        $entityManager->flush();
        return $this->redirect('/produits');
    }
}
