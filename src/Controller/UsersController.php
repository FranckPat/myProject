<?php

namespace App\Controller;

use App\Entity\Users;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
/**
 * @Route("/users")
 */
class UsersController extends Controller
{
    /**
     * @Route("/create")
     */
    public function create(Request $requestUsers){
        $entityManager = $this->getDoctrine()->getManager();
        $users = new Users();
        $form = $this->createFormBuilder($users)
            ->add('prenom',TextType::class)
            ->add('nom',TextType::class)
            ->add('age',IntegerType::class)
            ->add('EMail', TextareaType::class)
            ->add('submit',SubmitType::class)
            ->getForm();
        $form->handleRequest($requestUsers);
        $task = $form->getData();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($task);
            $entityManager->flush();
            return $this->redirect('/users');
        }


        return $this->render('users/create.html.twig', [
            'form' => $form->createView()
        ]);

    }
    /**
     * @Route("/")
     */
    public function index()
    {
        $users = $this->getDoctrine()->getRepository(Users::class)->findAll();
        return $this->render('users/index.html.twig', [
            'Users' => $users,
        ]);
    }
    /**
     * @Route("/edit/{id}")
     */
    public function update(Request $request, $id){
        $entityManager = $this->getDoctrine()->getManager();
        $users = $this->getDoctrine()->getRepository(Users::class)->find($id);
        $form = $this->createFormBuilder($users)
            ->add('nom', TextType::class)
            ->add('Prenom', TextType::class)
            ->add('age', IntegerType::class)
            ->add('Email', EmailType::class)
            ->add('save', SubmitType::class)
            ->getForm();//Récupération du formulaire
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            return $this->redirect('/users');
        }
        return $this->render('users/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }
    /**
     * @Route("/delete/{id}")
     */
    public function delete($id){
        $entityManager = $this->getDoctrine()->getManager();
        $product = $entityManager->getRepository(Users::class)->find($id);
        $entityManager->remove($product);
        $entityManager->flush();
        return $this->redirect('/users');
    }
}
